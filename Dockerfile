FROM ubuntu:20.04

# Install apt-add-repository and add support for HTTPs mirrors
RUN apt-get update
RUN apt-get install --allow-unauthenticated --yes curl software-properties-common apt-transport-https
# Install git and clone project tracking live (do manual clone & COPY if you want to freeze version)
RUN apt-get install -y git
RUN git clone https://github.com/desaster/kippo
